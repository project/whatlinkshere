<?php

namespace Drupal\whatlinkshere;

/**
 * @file
 * Link Manager class instance for handling references between content entities.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

class LinkManager implements LinkManagerInterface {

  /**
   * Drupal entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Path alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;


  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, EntityFieldManagerInterface $entity_field_manager, AliasManagerInterface $path_alias_manager,LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->pathAliasManager = $path_alias_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(EntityInterface $entity) {
    $fields = $this->entityFieldManager->getFieldDefinitions('node', $entity->bundle());

    $types_of_interest = [
      'entity_reference',
      'text_with_summary',
      'text_long',
      'link',
    ];

    // Array to store entity ids of things that this entity references.
    $reference_values = [];

    foreach ($fields as $field) {
      $type = $field->getType();
      $field_name = $field->getName();
      $field_value = NULL;

      // Skip over any fields we don't believe would contain a reference.
      if (in_array($type, $types_of_interest) == FALSE) {
        continue;
      }

      // Array to capture any extracted nids from any field type.
      $detected_entities = [];

      // Text fields that may contain link markup.
      if ($type == 'text_with_summary' || $type == 'text_long') {
        $field_value = $entity->get($field->getName())->value ?? '';

        $dom = Html::load($field_value);

        // Scan for link elements in this chunk of HTML.
        $link_elements = $dom->getElementsByTagName('a');

        foreach ($link_elements as $link) {
          $href = $link->getAttribute('href');

          if (preg_match('/^http/', $href)) {
            // Skip over absolute or external links.
            continue;
          }
          else {
            // Lookup content by path alias.
            $matches = [];
            preg_match('/(node|taxonomy\/term)\/(\d+)/', $this->pathAliasManager->getPathByAlias($href), $matches);
            // Fudge the path match for taxonomy term to the machine name of the entity type.
            $type = str_replace('taxonomy/term', 'taxonomy_term', $matches[1] ?? '');
            $entity_id = $matches[2] ?? 0;

            if (!empty($type && !empty($entity_id))) {
              $detected_entities[$type][] = $entity_id;
            }
          }
        };

        // Scan for embedded entities.
        $embed_elements = $dom->getElementsByTagName('drupal-entity');

        if (!empty($embed_elements)) {
          foreach ($embed_elements as $embed) {
            $entity_type = $embed->getAttribute('data-entity-type');
            $entity_uuid = $embed->getAttribute('data-entity-uuid');

            // Load the entity.
            $result = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['uuid' => $entity_uuid]);

            if (!empty($result)) {
              $embedded_entity = reset($result);
              $detected_entities[$entity_type][] = $embedded_entity->id();
            }
          }
        }

        // Scan for drupal-media elements.
        $drupal_media_elements = $dom->getElementsByTagName('drupal-media');

        if (!empty($drupal_media_elements)) {
          foreach ($drupal_media_elements as $embed) {
            $entity_type = $embed->getAttribute('data-entity-type');
            $entity_uuid = $embed->getAttribute('data-entity-uuid');

            // Load the entity.
            $result = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['uuid' => $entity_uuid]);

            if (!empty($result)) {
              $embedded_entity = reset($result);
              $detected_entities[$entity_type][] = $embedded_entity->id();
            }
          }
        }
      }

      // Any link fields.
      if ($type == 'link') {
        $link_field_values = $entity->get($field->getName())->getValue();

        foreach ($link_field_values as $link) {
          if (empty($link['uri'])) {
            continue;
          }

          $url = Url::fromUri($link['uri']);

          if ($url->isExternal() == FALSE) {
            try {
              $uri_path = $url->getInternalPath();
            }
            catch (\UnexpectedValueException $ex) {
              // TODO: pass params as placeholders rather than string interpolation.
              $this->logger->warning('Could not resolve internal path for Url (' . $link['uri'] . '), entity ID: ' . $entity->id());
              // Log the problem (likely a broken link) and continue the wider loop.
              continue 2;
            }

            $matches = [];

            if (preg_match('/node\/(\d+)/', $uri_path, $matches)) {
              $detected_entities['node'][] = $matches[1];
            }
          }
        }
      }

      // Entity reference fields (node / media).
      if ($type == 'entity_reference' || $type == 'media') {
        $field_value = $entity->get($field->getName())->getValue();
        $target_type = $field->getSetting('target_type');

        if (!empty($field_value) && in_array($target_type, ['node', 'media'])) {
          foreach ($field_value as $value) {
            $detected_entities[$target_type][] = $value['target_id'];
          }
        }
      }

      // Bundle the gathered values into an array to pass into our DB layer.
      foreach (array_keys($detected_entities) as $entity_type) {
        if (empty($detected_entities[$entity_type])) {
          continue;
        }

        foreach ($detected_entities[$entity_type] as $index => $id) {
          $reference_values[] = [
            'id' => $entity->id(),
            'entity_type' => $entity->getEntityTypeId(),
            'reference_id' => $id,
            'reference_entity_type' => $entity_type,
            'reference_field' => $field_name,
            'delta' => $index,
          ];
        }
      }
    }

    // Dedupe the reference values to insert to avoid any potential PK clashes.
    // As the reference values are multi-dimension arrays we can use this serialize + map
    // trick to weed out any duplicate values based on the entire array contents rather than just a key.
    // Serializing to a string will permit an easier comparison than checking each array key/value.
    $reference_values = array_map('unserialize', array_unique(array_map('serialize', ($reference_values))));

    // Delete existing values.
    $this->database->delete('whatlinkshere')
      ->condition('id', $entity->id())
      ->execute();

    // Insert new values.
    $query = $this->database->insert('whatlinkshere')->fields([
      'id',
      'entity_type',
      'reference_id',
      'reference_entity_type',
      'reference_field',
      'delta',
    ]);
    foreach ($reference_values as $record) {
      $query->values($record);
    }

    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEntity(EntityInterface $entity) {
    $this->database->delete('whatlinkshere')->condition('id', $entity->id())->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceContent(EntityInterface $entity, int $num_per_page, int $offset, array $sort_options = []) {
    $query = $this->database->select('node_field_data', 'nfd');
    $query->fields('nfd', ['nid', 'title', 'type']);
    $query->innerJoin('whatlinkshere', 'w', 'nfd.nid = w.id');
    $query->addExpression('GROUP_CONCAT(DISTINCT w.reference_field)', 'reference_fields');
    $query->condition('w.reference_id', $entity->id(), '=');
    $query->condition('w.reference_entity_type', $entity->getEntityTypeId(), '=');
    $query->groupBy('nfd.nid');
    $query->groupBy('nfd.title');
    $query->groupBy('nfd.type');
    $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($sort_options);
    $query->execute()->fetchAll();

    // First query excludes pager range; just for total result set size.
    $related_content = [
      'total' => count($query->execute()->fetchAll()),
    ];

    // Re-query with range (pager support and sort support).
    $query->range($offset, $num_per_page);
    $result = $query->execute()->fetchAll();

    foreach ($result as $record) {
      $related_content['rows'][] = [
        'nid' => $record->nid,
        'type' => $this->entityTypeManager->getStorage('node_type')->load($record->type)->label(),
        'title' => $record->title,
        // Pad commas with trailing space to improve readability.
        'reference_fields' => str_replace(',', ', ', $record->reference_fields),
      ];
    }

    return $related_content;
  }

}
