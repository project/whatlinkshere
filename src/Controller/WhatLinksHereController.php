<?php

namespace Drupal\whatlinkshere\Controller;

use Drupal\Core\Link;
use Drupal\node\NodeInterface;
use Drupal\media\MediaInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\whatlinkshere\LinkManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class WhatLinksHereController.
 *
 * Handles requests for routes defined in whatlinkshere.routing.yml.
 */
class WhatLinksHereController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\whatlinkshere\LinkManagerInterface definition.
   *
   * @var \Drupal\whatlinkshere\LinkManagerInterface
   */
  protected $linkManager;

  /**
   * Drupal\Core\StringTranslation\Translator\TranslatorInterface definition.
   *
   * @var \Drupal\Core\StringTranslation\Translator\TranslatorInterface
   */
  protected $t;

  /**
   * Drupal\Core\Pager\PagerManagerInterface definition.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Drupal\Core\Pager\PagerParametersInterface definition.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RequestStack $request, LinkManagerInterface $link_manager, TranslatorInterface $translator, PagerManagerInterface $pager_manager, PagerParametersInterface $pager_params) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
    $this->linkManager = $link_manager;
    $this->t = $translator;
    $this->pagerManager = $pager_manager;
    $this->pagerParameters = $pager_params;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('whatlinkshere.linkmanager'),
      $container->get('string_translation'),
      $container->get('pager.manager'),
      $container->get('pager.parameters')
    );
  }

  /**
   * Present a report of related content from the current node ID parameter.
   *
   * @param int $node
   *   Node ID of the node, defined in core/modules/node/node.routing.yml.
   * @return array
   *   Render array for Drupal to convert to HTML.
   */
  public function node(int $node) {
    $build = [];

    // Table header/sort options.
    $header = [
      'title' => [
        'data' => $this->t->translate('Title'),
        'field' => 'title',
        'sort' => 'asc'
      ],
      'type' => $this->t->translate('Type'),
      'fields' => $this->t->translate('From field(s)'),
      'tasks' => $this->t->translate('Tasks'),
    ];

    // Pager init.
    $page = $this->pagerParameters->findPage();
    $num_per_page = 25;
    $offset = $num_per_page * $page;

    // Fetch data about what content links to this node.
    $entity = $this->entityTypeManager->getStorage('node')->load($node);
    // Send a 404 if entity does not exist.
    if ($entity instanceof NodeInterface === FALSE) {
      throw new NotFoundHttpException();
    }
    $related_content = $this->linkManager->getReferenceContent($entity, $num_per_page, $offset, $header);

    // Now that we have the total number of results, initialize the pager.
    $this->pagerManager->createPager($related_content['total'], $num_per_page);

    $rows = [];

    if (!empty($related_content['rows'])) {
      foreach ($related_content['rows'] as $item) {
        $rows[] = [
          Link::createFromRoute($item['title'], 'entity.node.canonical', ['node' => $item['nid']]),
          $item['type'],
          $item['reference_fields'],
          Link::createFromRoute($this->t->translate('Edit'), 'entity.node.edit_form', ['node' => $item['nid']]),
        ];
      }
    }

    $build['links_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t->translate('No content links here.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    // Handoff to alter hook so other modules can adjust the render array.
    \Drupal::moduleHandler()->invokeAll('whatlinkshere_report_alter', [&$build]);

    return $build;
  }

  /**
   * Present a report of related content from the current taxonomy term ID parameter.
   *
   * @param int $taxonomy_term
   *   Taxonomy term ID.
   * @return array
   *   Render array for Drupal to convert to HTML.
   */
  public function taxonomyTerm(int $taxonomy_term) {
    $build = [];

    // Table header/sort options.
    $header = [
      'title' => [
        'data' => $this->t->translate('Title'),
        'field' => 'title',
        'sort' => 'asc'
      ],
      'type' => $this->t->translate('Type'),
      'fields' => $this->t->translate('From field(s)'),
      'tasks' => $this->t->translate('Tasks'),
    ];

    // Pager init.
    $page = $this->pagerParameters->findPage();
    $num_per_page = 25;
    $offset = $num_per_page * $page;

    // Fetch data about what content links to this node.
    $entity = $this->entityTypeManager->getStorage('taxonomy_term')->load($taxonomy_term);
    // Send a 404 if term does not exist.
    if ($entity instanceof TermInterface === FALSE) {
      throw new NotFoundHttpException();
    }
    $related_content = $this->linkManager->getReferenceContent($entity, $num_per_page, $offset, $header);

    // Now that we have the total number of results, initialize the pager.
    $this->pagerManager->createPager($related_content['total'], $num_per_page);

    $rows = [];

    if (!empty($related_content['rows'])) {
      foreach ($related_content['rows'] as $item) {
        $rows[] = [
          Link::createFromRoute($item['title'], 'entity.node.canonical', ['node' => $item['nid']]),
          $item['type'],
          $item['reference_fields'],
          Link::createFromRoute($this->t->translate('Edit'), 'entity.node.edit_form', ['node' => $item['nid']]),
        ];
      }
    }

    $build['links_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t->translate('No content links here.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    // Handoff to alter hook so other modules can adjust the render array.
    \Drupal::moduleHandler()->invokeAll('whatlinkshere_report_alter', [&$build]);

    return $build;
  }

  /**
   * Present a report of related content from the current media ID parameter.
   *
   * @param int $media
   *   Media ID.
   * @return array
   *   Render array for Drupal to convert to HTML.
   */
  public function media(int $media) {
    $build = [];

    // Table header/sort options.
    $header = [
      'title' => [
        'data' => $this->t->translate('Title'),
        'field' => 'title',
        'sort' => 'asc'
      ],
      'type' => $this->t->translate('Type'),
      'fields' => $this->t->translate('From field(s)'),
      'tasks' => $this->t->translate('Tasks'),
    ];

    // Pager init.
    $page = $this->pagerParameters->findPage();
    $num_per_page = 25;
    $offset = $num_per_page * $page;

    // Fetch data about what content links to this node.
    $entity = $this->entityTypeManager->getStorage('media')->load($media);
    // Send a 404 if media does not exist.
    if ($entity instanceof MediaInterface === FALSE) {
      throw new NotFoundHttpException();
    }
    $related_content = $this->linkManager->getReferenceContent($entity, $num_per_page, $offset, $header);

    // Now that we have the total number of results, initialize the pager.
    $this->pagerManager->createPager($related_content['total'], $num_per_page);

    $rows = [];

    if (!empty($related_content['rows'])) {
      foreach ($related_content['rows'] as $item) {
        $rows[] = [
          Link::createFromRoute($item['title'], 'entity.node.canonical', ['node' => $item['nid']]),
          $item['type'],
          $item['reference_fields'],
          Link::createFromRoute($this->t->translate('Edit'), 'entity.node.edit_form', ['node' => $item['nid']]),
        ];
      }
    }

    $build['links_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t->translate('No content links here.'),
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    // Handoff to alter hook so other modules can adjust the render array.
    \Drupal::moduleHandler()->invokeAll('whatlinkshere_report_alter', [&$build]);

    return $build;
  }

}
