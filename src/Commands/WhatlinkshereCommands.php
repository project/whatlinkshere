<?php

namespace Drupal\whatlinkshere\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\whatlinkshere\LinkManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class WhatlinkshereCommands extends DrushCommands {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Link manager service.
   *
   * @var \Drupal\whatlinkshere\LinkManagerInterface
   */
  protected $linkManager;

  /**
   * Constructs a new DefaultCommand object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, LinkManagerInterface $link_manager) {
    parent::__construct();

    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->linkManager = $link_manager;
  }

  /**
   * Scan nodes for related content.
   *
   * @param array $options
   *   (optional) An array of options.
   *
   * @command whatlinkshere:scan --nid=node_id
   *
   * @usage whatlinkshere:scan
   *   Scan all content for related content.
   */
  public function scan(array $options = ['nid' => NULL]) {
    // 1. Log the start of the script.
    $this->logger()->info(dt('Content scan operation started.'));

    // 2. Retrieve all nodes.
    try {
      $storage = $this->entityTypeManager->getStorage('node');
      $query = $storage->getQuery();

      if (!empty($options['nid'])) {
        $query->condition('nid', $options['nid']);
      }

      $query->accessCheck(FALSE);
      $nids = $query->execute();
    }
    catch (\Exception $e) {
      $this->output()->writeln($e);
      $this->logger()->error(dt('Error found @e', ['@e' => $e]));
    }

    // 3. Create the operations array for the batch.
    $operations = [];
    $numOperations = 0;
    $batchId = 1;
    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $operations[] = [
          '\Drupal\whatlinkshere\BatchService::process',
          [
            $batchId,
            t('Scanning node @nid', ['@nid' => $nid]),
          ],
        ];
        $batchId++;
        $numOperations++;
      }
    }
    else {
      $this->logger()->warning('No node(s) detected');
    }
    // 4. Create the batch.
    $batch = [
      'title' => t('Updating @num node(s)', ['@num' => $numOperations]),
      'operations' => $operations,
      'finished' => '\Drupal\whatlinkshere\BatchService::processFinished',
    ];

    // 5. Add batch operations as new batch sets.
    batch_set($batch);
    // 6. Process the batch sets.
    drush_backend_batch_process();
    // 6. Show some information.
    $this->logger()->notice("Content scan operations finished.");
    // 7. Log some information.
    $this->logger()->info('Content scan operations end.');
  }

}
